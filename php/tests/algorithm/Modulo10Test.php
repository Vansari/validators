<?php
declare (strict_types = 1);

namespace algorithm;

use ErrorException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \algorithm\Modulo10
 * @author Kurt Junker
 *
 */
class Modulo10Test extends TestCase
{

    public function getFailedNumerics(): array
    {
        return [
            [
                '',
            ],
            [
                'abcdef',
            ],
            [
                '123456789O',
            ],
            [
                '0x1078FF',
            ],
        ];
    }

    public function getTestNumerics(): array
    {
        return [
            [
                'in' => '1134',
                'checkDigit' => 6,
            ],
            [
                'in' => '9',
                'checkDigit' => 1,
            ],
            [
                'in' => '98',
                'checkDigit' => 4,
            ],
            [
                'in' => '987',
                'checkDigit' => 8
            ],
            [
                'in' => '9876',
                'checkDigit' => 4,
            ],
            [
                'in' => '57493514789578511235845',
                'checkDigit' => 9,
            ],
        ];
    }

    /**
     * @dataProvider getTestNumerics
     * @covers ::getDigit
     *
     * @param string $numeric
     * @param int $checkDigit
     * @throws ErrorException
     */
    public function testGetDigit(string $numeric, int $checkDigit): void
    {
        $this->assertSame($checkDigit, Modulo10::getDigit($numeric));
    }

    /**
     * @dataProvider getTestNumerics
     * @covers ::isValid
     * @covers ::getDigit
     *
     * @param string $numeric
     * @param int $checkDigit
     * @throws ErrorException
     */
    public function testCheck(string $numeric, int $checkDigit): void
    {
        $this->assertSame($checkDigit, Modulo10::getDigit($numeric));
        $this->assertTrue(Modulo10::check($numeric . $checkDigit));
    }

    /**
     * @dataProvider getFailedNumerics
     * @covers ::getDigit
     *
     * @param string $failedNumeric
     * @throws ErrorException
     */
    public function testGetDigitFailedWithNonNumericInput(
        string $failedNumeric
    ): void {
        $this->expectException(\InvalidArgumentException::class);
        Modulo10::getDigit($failedNumeric);
    }

    /**
     * @dataProvider getFailedNumerics
     * @covers ::isValid
     *
     * @param string $failedNumeric
     * @throws ErrorException
     */
    public function testIsValidFailedWithNonNumericInput(
        string $failedNumeric
    ): void {
        $this->expectException(\InvalidArgumentException::class);
        Modulo10::check($failedNumeric);
    }
}
