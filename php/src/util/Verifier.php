<?php
declare(strict_types = 1);

namespace util;

use InvalidArgumentException;
use util\exception\PregException;

class Verifier
{
    /**
     * @param string $input
     * @throws PregException - If the Regex_match Function crashed
     * @throws InvalidArgumentException - If input is an not expected argument
     */
    public static function verifyAlphanumericString(string $input): void
    {
        if ('' === $input) {
            throw new InvalidArgumentException('Input must be an non empty string.');
        }

        if (0 === Preg::match('/^[A-Za-z0-9]+$/u', $input)) {
            throw new InvalidArgumentException('Input is not an valid alphanumeric string!');
        }
    }

    /**
     * Guard to Check that the given Input is a valid numeric string
     * Allowed are digits from 0-9 with n-length
     *
     * @param string $input
     * @throws PregException
     * @throws InvalidArgumentException
     */
    public static function verifyBase10Numeric(string $input): void
    {
        if ('' === $input || !is_numeric($input)) {
            throw new InvalidArgumentException('Input must be an non empty numeric string!');
        }

        if (0 === Preg::match('/^[0-9]+$/', $input)) {
            throw new InvalidArgumentException('Invalid numeric string!');
        }
    }

    /**
     * @param string $input
     * @return void
     * @throws InvalidArgumentException
     */
    public static function verifyNonEmptyString(string $input): void
    {
        if (is_string($input) && '' !== $input) {
            return;
        }
        throw new InvalidArgumentException('Input must be a non empty string!');
    }
}
