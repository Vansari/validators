<?php
declare(strict_types = 1);

namespace util;

use InvalidArgumentException;

class Converter
{
    /**
     * Converts a alphanumeric input string to a numeric string
     * @param string $input
     * @return string
     * @throws InvalidArgumentException
     */
    public static function convertToNumeric(string $input): string
    {
        Verifier::verifyNonEmptyString($input);
        $result = '';
        foreach (str_split($input) as $char) {
            $result .= base_convert($char, 36, 10);
        }

        return $result;
    }
}