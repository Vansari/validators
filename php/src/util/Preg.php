<?php
declare(strict_types = 1);

namespace util;

use util\exception\PregException;

class Preg
{
    /**
     * @param string $pattern
     * @param string $subject
     * @param array $matches
     * @param int $flags
     * @param int $offset
     * @return int
     * @throws PregException
     */
    public static function match(
        string $pattern,
        string $subject,
        ?array &$matches = [],
        int $flags = 0,
        int $offset = 0
    ): int {
        error_clear_last();
        $result = @preg_match($pattern, $subject, $matches, $flags, $offset);
        if (false === $result) {
            throw new PregException('Regex pattern crashed!', preg_last_error());
        }

        return $result;
    }
}