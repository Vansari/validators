<?php
declare (strict_types = 1);

namespace algorithm;

interface AlgorithmInterface
{
    /**
     * Validates the given $numeric string if it is valid by the algorithm
     * @param string $numeric
     * @return bool true if it is valid otherwise false
     */
    public static function check(string $numeric): bool;
}
