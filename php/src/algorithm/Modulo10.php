<?php
declare (strict_types = 1);

namespace algorithm;

use InvalidArgumentException;
use util\exception\PregException;
use util\Verifier;

/**
 * Class Modulo10 - This class checks if a numeric string has a
 * valid check digit at the end
 * Validated by modulus 10  (known as Luhn Algorithm)
 *
 * @author Kurt Junker
 */
class Modulo10 implements AlgorithmInterface
{

    /**
     * Calculates the check digit of the given numeric string
     * @param string $numeric - The numeric string without check digit
     * @return int The calculated check digit
     * @throws PregException
     * @throws InvalidArgumentException
     */
    public static function getDigit(string $numeric): int
    {
        Verifier::verifyBase10Numeric($numeric);
        $checksum = self::calculate($numeric . '0');

        return (10 - $checksum) % 10;
    }

    /**
     * Validates the given $numeric string if it is valid by the algorithm
     * @param string $numeric
     * @return bool true if it is valid otherwise false
     */
    public static function check(string $numeric): bool
    {
        try {
            Verifier::verifyBase10Numeric($numeric);
            $checksum = self::calculate($numeric);

            return 0 === $checksum;
        } catch (PregException | InvalidArgumentException $exception) {
            return false;
        }
    }

    /**
     * Creates a checkDigit and appends it at the end of
     * the given $numeric string
     * @param string $numeric
     * @param bool $soft - Do not append Digit if $numeric is already valid
     * @return string
     * @throws PregException
     * @throws InvalidArgumentException
     */
    public static function create(string $numeric, bool $soft = false): string
    {
        Verifier::verifyBase10Numeric($numeric);
        return !self::check($numeric) || !$soft
            ? $numeric . self::getDigit($numeric)
            : $numeric;
    }

    /**
     * Calculates the Checksum by Modulus 10 Algorithm
     * @param string $numeric
     * @return int
     */
    private static function calculate(string $numeric): int
    {
        $result = '';
        $length = strlen($numeric);
        $revNumeric = strrev($numeric);
        for ($i = 0; $i < $length; $i++) {
            $result .= ($i & 1) ? $revNumeric{$i} * 2 : $revNumeric{$i};
        }

        return (array_sum(str_split($result)) % 10);
    }
}

