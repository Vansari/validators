<?php
declare (strict_types = 1);

namespace algorithm;

use InvalidArgumentException;
use util\exception\PregException;
use util\Verifier;

class Modulo97Ten implements AlgorithmInterface
{

    /**
     * Calculates the check digit of the given numeric string
     * @param string $numeric - The numeric string without check digit
     * @return string The calculated check digit as string.
     * If it is smaller than 10 then with leading zero.
     * @throws PregException
     * @throws InvalidArgumentException
     */
    public static function getDigit(string $numeric): string
    {
        Verifier::verifyBase10Numeric($numeric);
        $result = (98 - self::calculate($numeric . '00'));

        return (string)((10 > $result) ? '0' . $result : $result);
    }

    /**
     * Validates the given $numeric string if it is valid by the algorithm
     * @param string $numeric
     * @return bool true if it is valid otherwise false
     */
    public static function check(string $numeric): bool
    {
        try {
            Verifier::verifyBase10Numeric($numeric);

            return 1 === self::calculate($numeric);
        } catch (PregException | InvalidArgumentException $exception) {
            return false;
        }
    }

    /**
     * Creates a checkDigit and appends it at the end of
     * the given $numeric string
     * @param string $numeric
     * @param bool $soft - Do not append Digit if $numeric is already valid
     * @return string
     * @throws PregException
     * @throws InvalidArgumentException
     */
    public static function create(string $numeric, bool $soft = false): string
    {
        Verifier::verifyBase10Numeric($numeric);
        return !self::check($numeric) || !$soft
            ? $numeric . self::getDigit($numeric)
            : $numeric;
    }

    /**
     * Calculates the checksum with Modulo97
     * @param string $numeric
     * @return int - The calculated checksum
     */
    private static function calculate(string $numeric): int
    {
        $offset = 0;
        $length = 9;
        $result = 0;

        while (false !== ($part = substr($numeric, $offset, $length))) {
            $num = $result . $part;
            $offset += $length;
            $length = 7;

            $result = (int)bcmod($num, '97');
        }

        return $result;
    }
}
